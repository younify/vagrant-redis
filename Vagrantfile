VAGRANTFILE_API_VERSION = "2"

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|
    config.vm.box = "bento/ubuntu-16.04"

    config.ssh.username = 'vagrant'
    config.ssh.password = 'vagrant'
    config.ssh.insert_key = false


    if Vagrant.has_plugin?("vbguest")
        config.vbguest.auto_reboot = true
        config.vbguest.no_install = false
    end

    if Vagrant.has_plugin?("vagrant-cachier")
        config.cache.scope = :box
    end

    config.vm.define "build-redis" do |machine|
        machine.vm.network "private_network", ip: "172.17.177.2"
        machine.vm.provider :virtualbox do |vbox|
            vbox.customize ["modifyvm", :id, "--memory", 1024]
            vbox.name = 'build-redis'
            vbox.linked_clone = true
        end
    end

    config.vm.define 'controller' do |machine|
        machine.vm.network "private_network", ip: "172.17.177.254"

        machine.vm.provider :virtualbox do |vbox|
            vbox.customize ["modifyvm", :id, "--memory", 2048]
            vbox.name = 'build-controller'
            vbox.linked_clone = true
        end

        Vagrant.configure("2") do |config|
            config.vm.provision "shell", inline: "sudo apt-get -y update && sudo apt-get install -y sshpass"
        end

        machine.vm.provision :ansible_local do |ansible|
            ansible.install_mode = "pip"
            ansible.playbook = "playbook.yml"
            ansible.verbose = true
            ansible.install = true
            ansible.limit = "nodes"
            ansible.inventory_path = "inventory"
            ansible.galaxy_role_file = "requirements.yml"
            ansible.galaxy_roles_path = ".roles"
        end
    end
end
