#!/bin/bash

rm -rf .vagrant

yes | vagrant box remove --all test-redis
vagrant box add --name test-redis ../build/$1
vagrant up
vagrant ssh -c "redis-cli INFO"
yes | vagrant destroy
yes | vagrant box remove --all test-redis
vagrant box prune
