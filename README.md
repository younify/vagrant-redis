# Simple Redis box builder

## How to use this box

```ruby
VAGRANTFILE_API_VERSION = "2"
GUEST_IP = '172.16.3.2'
GUEST_HOSTNAME = 'redis.internal'

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|
  config.vm.box = "younify/redis"
  config.vm.hostname = GUEST_HOSTNAME
  config.vm.network :private_network, ip: GUEST_IP
  config.vm.synced_folder ".", "/vagrant", disabled: true

  config.vm.provider :virtualbox do |vbox|
    vbox.customize ["modifyvm", :id, "--memory", 1024]
    vbox.name = GUEST_HOSTNAME
    vbox.linked_clone = true
  end
end
```
