#!/bin/bash
VERSION="1.0.3"

NAME="younify/redis"
VM_NAME="build-redis"
BOX_PATH="build/redis-v${VERSION}.virtualbox.box"
BOX_NAME=`basename ${BOX_PATH}`

function define(){ IFS='\n' read -r -d '' ${1} || true; }

rm -rf .vagrant
vagrant up --provision
vagrant halt
vagrant package --base ${VM_NAME} --output ${BOX_PATH}

chk=`sha1sum -b ${BOX_PATH} | awk '{ print $1 }'`

define metadata <<EOF
{
    "name": "${NAME}",
    "versions": [{
        "version": "${VERSION}",
        "providers": [
            {
                "name": "virtualbox",
                "url": "http://vagrant.younifydev.net/${NAME}/${BOX_NAME}",
                "checksum_type": "sha1",
                "checksum": "${chk}"
            }
        ]
    }]
}
EOF

echo "${metadata}" > ${BOX_PATH}.json
